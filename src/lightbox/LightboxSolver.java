package lightbox;

public class LightboxSolver
{
	private State initial;
	private final State[] switches = new State[LightBox.COMBINATIONS_POWER];

	static boolean isBitSet(int num, int bit)
	{
		return ((num >>> bit) & 1) != 0;
	}

	private static boolean isLit(State s)
	{
		for (int i = 0; i < LightBox.WIDTH; ++i)
		{
			for (int j = 0; j < LightBox.HEIGHT; ++j)
			{
				if (!s.getState(i, j))
				{
					return false;
				}
			}
		}
		return true;
	}

	public Solution solve()
	{
		for (int i = 0; i < Math.pow(2, LightBox.COMBINATIONS_POWER); ++i)
		{
			State s = initial;

			for (int bit = 0; bit < LightBox.COMBINATIONS_POWER; ++bit)
			{
				if (isBitSet(i, bit))
				{
					s = s.flip(switches[bit]);
				}
			}

			if (isLit(s))
			{
				return new Solution(i);
			}
		}

		return null;
	}

	public void setInitial(State initial)
	{
		this.initial = initial;
	}

	public void setSwitchChange(Combination combination, State newState)
	{
		switches[combination.ordinal()] = initial.diff(newState);
		setInitial(newState);
	}
}

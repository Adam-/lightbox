package lightbox;

public class Solution
{
	private int solution;

	public Solution()
	{
	}

	public Solution(int solution)
	{
		this.solution = solution;
	}

	public boolean getFlip(Combination combination)
	{
		return LightboxSolver.isBitSet(solution, combination.ordinal());
	}

	public void flip(Combination c)
	{
		solution |= (1 << c.ordinal());
	}

	@Override
	public int hashCode()
	{
		int hash = 7;
		hash = 79 * hash + this.solution;
		return hash;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
		{
			return true;
		}
		if (obj == null)
		{
			return false;
		}
		if (getClass() != obj.getClass())
		{
			return false;
		}
		final Solution other = (Solution) obj;
		if (this.solution != other.solution)
		{
			return false;
		}
		return true;
	}
}

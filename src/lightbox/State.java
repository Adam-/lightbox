package lightbox;

public class State
{
	private final boolean[][] state = new boolean[LightBox.WIDTH][LightBox.HEIGHT];

	public void setState(int x, int y, boolean s)
	{
		state[x][y] = s;
	}

	public boolean getState(int x, int y)
	{
		return state[x][y];
	}

	public State diff(State other)
	{
		State newState = new State();

		for (int i = 0; i < LightBox.WIDTH; ++i)
		{
			for (int j = 0; j < LightBox.HEIGHT; ++j)
			{
				newState.state[i][j] = state[i][j] ^ other.state[i][j];
			}
		}

		return newState;
	}

	public State flip(State other)
	{
		State newState = new State();

		for (int i = 0; i < LightBox.WIDTH; ++i)
		{
			for (int j = 0; j < LightBox.HEIGHT; ++j)
			{
				newState.state[i][j] = other.state[i][j] ? !state[i][j] : state[i][j];
			}
		}

		return newState;
	}
}

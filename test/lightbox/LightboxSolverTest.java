package lightbox;

import org.junit.Assert;
import org.junit.Test;

public class LightboxSolverTest
{
	private static final int[] INITIAL = new int[] {
		1,0,1,0,0,
		0,1,0,1,0,
		0,1,1,1,0,
		0,1,0,1,0,
		1,0,1,0,1
	};

	private static final int[] A = new int[] {
		0,1,0,0,0,
		0,0,0,0,1,
		0,1,0,1,0,
		0,0,0,1,0,
		0,0,0,0,1
	};

	private static final int[] B = new int[] {
		0,1,0,0,0,
		1,0,0,0,1,
		0,0,0,1,0,
		1,1,0,1,0,
		0,0,0,1,1,
	};

	private static final int[] C = new int[] {
		0,1,0,0,0,
		1,0,0,0,1,
		1,1,0,0,0,
		0,1,0,0,0,
		0,0,0,0,1,
	};

	private static final int[] D = new int[] {
		1,1,0,0,0,
		1,0,1,0,1,
		1,1,0,1,1,
		0,1,1,0,0,
		1,0,0,1,1,
	};

	private static final int[] E = new int[] {
		1,0,0,1,0,
		1,1,1,0,1,
		1,1,0,1,0,
		0,0,1,0,0,
		1,0,0,1,1,
	};

	private static final int[] F = new int[] {
		1,0,0,1,0,
		1,0,0,0,1,
		1,0,1,1,0,
		0,0,1,0,0,
		1,0,0,0,0,
	};

	private static final int[] G = new int[] {
		1,0,0,1,1,
		1,1,1,0,0,
		0,1,1,1,0,
		0,0,0,0,1,
		1,1,0,0,0,
	};

	private static final int[] H = new int[] {
		1,0,1,1,1,
		1,1,1,0,0,
		0,1,1,1,0,
		1,1,0,0,1,
		1,0,0,1,0
	};

	private State fromArray(int[] array)
	{
		State s = new State();

		assert array.length == 25;
		for (int i = 0; i < array.length; ++i)
			s.setState(i / 5, i % 5, array[i] != 0);

		return s;
	}

	@Test
	public void test()
	{
		LightboxSolver solver = new LightboxSolver();
		
		solver.setInitial(fromArray(INITIAL));
		solver.setSwitchChange(Combination.A, fromArray(A));
		solver.setSwitchChange(Combination.B, fromArray(B));
		solver.setSwitchChange(Combination.C, fromArray(C));
		solver.setSwitchChange(Combination.D, fromArray(D));
		solver.setSwitchChange(Combination.E, fromArray(E));
		solver.setSwitchChange(Combination.F, fromArray(F));
		solver.setSwitchChange(Combination.G, fromArray(G));
		solver.setSwitchChange(Combination.H, fromArray(H));

		Solution solution = solver.solve();

		Solution expected = new Solution();
		expected.flip(Combination.A);
		expected.flip(Combination.C);
		expected.flip(Combination.D);
		expected.flip(Combination.F);
		expected.flip(Combination.H);
		
		Assert.assertEquals(expected, solution);
	}

	@Test
	public void testPrint()
	{
		LightboxSolver solver = new LightboxSolver();

		solver.setInitial(fromArray(INITIAL));
		solver.setSwitchChange(Combination.A, fromArray(A));
		solver.setSwitchChange(Combination.B, fromArray(B));
		solver.setSwitchChange(Combination.C, fromArray(C));
		solver.setSwitchChange(Combination.D, fromArray(D));
		solver.setSwitchChange(Combination.E, fromArray(E));
		solver.setSwitchChange(Combination.F, fromArray(F));
		solver.setSwitchChange(Combination.G, fromArray(G));
		solver.setSwitchChange(Combination.H, fromArray(H));

		Solution solution = solver.solve();

		for (Combination c : Combination.values())
			if (solution.getFlip(c))
				System.out.println(c);
	}

}
